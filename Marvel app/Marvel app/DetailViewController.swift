//
//  DetailViewController.swift
//  Marvel app
//
//  Created by Danilo Angamarca on 3/1/18.
//  Copyright © 2018 kevin. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var pokemon:Pokemon?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(pokemon?.name)
    }

}

//
//  PokemonTableViewCell.swift
//  Marvel app
//
//  Created by Danilo Angamarca on 3/1/18.
//  Copyright © 2018 kevin. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func FillData(pokemon:Pokemon){
        
        NameLabel.text = pokemon.name
        heightLabel.text = "\(pokemon.height ?? 0)"
        weightLabel.text = "\(pokemon.weight ?? 0)"
        
    }

}
